# Sales Incentives (PHP)

Repository for the PHP code used by the &#34;Sales Incentives App&#34; developed in SFDC.

* http://tgweb.technogym.com/wikis/ced/index.php/Sales_Incentives
* http://tgweb.technogym.com/wikis/ced/index.php/Sales_Incentives_2.0
* http://tgweb.technogym.com/wikis/ced/index.php/Sales_Incentives_2.0_Management_Console